# O_Test


## Project Description
This is an web ASP.NET application with Adventure Works db (https://learn.microsoft.com/en-us/sql/samples/adventureworks-install-configure?view=sql-server-ver16&tabs=ssms). 

There is 2 endpoints - to download diagram of entities and FKs only of this db as a pdf and as an html.

In PDF relationships are described as static text. In HTML you can hower over an arrow to see this text (By default it is hidden).

## Demo
~
![Alt text](https://snipboard.io/7ZF5q6.jpg "Swagger")

~
![Alt text](https://snipboard.io/Fpts23.jpg "Pdf")

~
![Alt text](https://snipboard.io/DTVgx0.jpg "Html")

## Required Software to be installed on server
- PlantUml
- ImageMagick