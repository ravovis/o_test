using Microsoft.AspNetCore.Mvc;
using OnixTestWeb.Services;

namespace OnixTestWeb.Controllers
{
    [ServiceFilter(typeof(LoggingActionFilter))]
    [ApiController]
    [Route("[controller]")]
    public class DiagramController : ControllerBase
    {
        
        private readonly IDiagramService _diagramService;
        private readonly ILogger<DiagramController> _logger;

        public DiagramController(ILogger<DiagramController> logger, IDiagramService diagramService)
        {
            _logger = logger;
            _diagramService = diagramService;
        }

        [HttpGet("pdf")]
        public async Task<IActionResult> GetPdf()
        {
            byte[] pdf = await _diagramService.GetDbDiagramAsPdf();
            return File(pdf, "application/pdf", "diagram.pdf");
        }

        [HttpGet("html")]
        public async Task<IActionResult> GetHtml()
        {
            byte[] html = await _diagramService.GetDbDiagramAsHtml();
            return File(html, "text/html", "diagram.html");
        }
    }
}
