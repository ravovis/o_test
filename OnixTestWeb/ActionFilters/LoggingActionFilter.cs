﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;

public class LoggingActionFilter : IAsyncActionFilter
{
    private readonly ILogger<LoggingActionFilter> _logger;

    public LoggingActionFilter(ILogger<LoggingActionFilter> logger)
    {
        _logger = logger;
    }

    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var actionName = context.ActionDescriptor.DisplayName;
        _logger.LogInformation($"Starting execution of {actionName}");

        var stopwatch = Stopwatch.StartNew();

        var resultContext = await next();

        stopwatch.Stop();
        var duration = stopwatch.Elapsed;

        _logger.LogInformation($"Finished execution of {actionName}. Duration: {duration.TotalMilliseconds} ms");
    }
}
