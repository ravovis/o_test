﻿namespace OnixTestWeb.Services;

public interface IDiagramService
{
    public Task<byte[]> GetDbDiagramAsPdf();
    public Task<byte[]> GetDbDiagramAsHtml();
}
