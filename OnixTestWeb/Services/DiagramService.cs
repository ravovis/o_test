﻿using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OnixTest2.Models;
using System.Diagnostics;
using System.Text;

namespace OnixTestWeb.Services;

public class DiagramService : IDiagramService
{
    private const string PlantUmlJarPath = "C:\\Users\\User\\Downloads\\plantuml-jar-gplv2-1.2023.7\\plantuml.jar";
    private const string ImageMagickPath = "\"C:\\Program Files\\ImageMagick-7.1.1-Q16-HDRI\\magick.exe\"";

    public async Task<byte[]> GetDbDiagramAsHtml()
    {
        return await GenerateDiagramAsync(Format.Html);
    }

    public async Task<byte[]> GetDbDiagramAsPdf()
    {
        return await GenerateDiagramAsync(Format.Pdf);
    }

    private async Task<byte[]> GenerateDiagramAsync(Format format)
    {
        var model = GetDbModel();
        var types = new List<string>();
        var plantUmlContent = GeneratePlantUmlContent(model, types);

        var plantUmlBytes = Encoding.UTF8.GetBytes(plantUmlContent);
        using var plantUmlStream = new MemoryStream(plantUmlBytes);

        return format switch
        {
            Format.Html => CreateHtmlFile(types, RunPlantUml(plantUmlStream, "-tsvg")),
            Format.Pdf => ConvertPngToPdf(RunPlantUml(plantUmlStream, "-tpng")),
            _ => throw new ArgumentException("Invalid format specified")
        };
    }

    private IModel GetDbModel()
    {
        using var context = new AdventureWorksContext();
        return context.Model;
    }

    private string GeneratePlantUmlContent(IModel model, List<string> types)
    {
        var plantUmlContent = new StringWriter();
        plantUmlContent.WriteLine("@startuml");
        AddHeaderStyle(plantUmlContent);
        plantUmlContent.WriteLine("hide stereotype");

        foreach (var entityType in model.GetEntityTypes())
        {
            AddEntityTypeToPlantUmlContent(entityType, plantUmlContent);
        }

        foreach (var entityType in model.GetEntityTypes())
        {
            AddEntityRelationshipsToPlantUmlContent(entityType, plantUmlContent, types);
        }

        plantUmlContent.WriteLine("@enduml");
        return plantUmlContent.ToString();
    }

    private void AddHeaderStyle(StringWriter plantUmlContent)
    {
        plantUmlContent.WriteLine("<style>");
        plantUmlContent.WriteLine("header {");
        plantUmlContent.WriteLine("  HorizontalAlignment center");
        plantUmlContent.WriteLine("  FontSize 26");
        plantUmlContent.WriteLine("  FontColor black");
        plantUmlContent.WriteLine("  BackgroundColor lightblue");
        plantUmlContent.WriteLine("}");
        plantUmlContent.WriteLine("</style>");
    }

    private void AddEntityTypeToPlantUmlContent(IEntityType entityType, StringWriter plantUmlContent)
    {
        var tableName = GetTableName(entityType);
        if (!string.IsNullOrEmpty(tableName))
        {
            plantUmlContent.WriteLine($"class {tableName} <<header>> {{");
            foreach (var property in entityType.GetProperties())
            {
                if (property.IsForeignKey())
                    plantUmlContent.WriteLine($"  {property.Name}");
            }
            plantUmlContent.WriteLine("}");
        }
    }

    private void AddEntityRelationshipsToPlantUmlContent(IEntityType entityType, StringWriter plantUmlContent, List<string> types)
    {
        var tableName = GetTableName(entityType);
        foreach (var foreignKey in entityType.GetForeignKeys())
        {
            var principalTable = GetTableName(foreignKey.PrincipalEntityType);
            if (!string.IsNullOrEmpty(principalTable))
            {
                var cardinalityInfo = $"{tableName} to {principalTable}";
                types.Add(cardinalityInfo);
                plantUmlContent.WriteLine($"{tableName} --> {principalTable} : {cardinalityInfo}");
            }
        }
    }

    private static byte[] CreateHtmlFile(IEnumerable<string> types, byte[] svgBytes)
    {
        var svgContent = Encoding.UTF8.GetString(svgBytes);
        var htmlDoc = new HtmlDocument();
        htmlDoc.LoadHtml(svgContent);

        AddDataInfoToPaths(htmlDoc, types);
        HideTextInLinks(htmlDoc);

        string annotatedSvgContent = htmlDoc.DocumentNode.OuterHtml;
        return GenerateHtmlContent(annotatedSvgContent);
    }

    private static byte[] GenerateHtmlContent(string annotatedSvgContent)
    {
        string htmlContent = $@"
<!DOCTYPE html>
<html lang=""en"">
<head>
    <meta charset=""UTF-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Interactive Database Schema</title>
    <script src=""https://d3js.org/d3.v6.min.js""></script>
    <style>
        .tooltip {{
            position: absolute;
            text-align: center;
            width: auto;
            height: auto;
            padding: 2px;
            font: 12px sans-serif;
            background: lightsteelblue;
            border: 0px;
            border-radius: 8px;
            pointer-events: none;
        }}
        .header {{
            background-color: lightblue;
            font-size: 14px;
            padding: 2px;
        }}
        .field {{
            background-color: white;
            font-size: 12px;
            padding: 2px;
        }}
    </style>
</head>
<body>
    <div id=""diagram"">
        {annotatedSvgContent}
    </div>
    <script>
        const tooltip = d3.select(""body"").append(""div"").attr(""class"", ""tooltip"").style(""opacity"", 0);

        d3.selectAll(""path[data-info]"").on(""mouseover"", function(event, d) {{
            const relationInfo = d3.select(this).attr('data-info');
            tooltip.transition().duration(200).style(""opacity"", .9);
            tooltip.html(relationInfo)
                .style(""left"", (event.pageX) + ""px"")
                .style(""top"", (event.pageY - 28) + ""px"");
        }}).on(""mouseout"", function(d) {{
            tooltip.transition().duration(500).style(""opacity"", 0);
        }});
    </script>
</body>
</html>";

        return Encoding.UTF8.GetBytes(htmlContent);
    }

    private static void AddDataInfoToPaths(HtmlDocument htmlDoc, IEnumerable<string> types)
    {
        var pathNodesWithId = htmlDoc.DocumentNode.SelectNodes("//path[@id]").ToList();
        for (int i = 0; i < pathNodesWithId.Count && i < types.Count(); i++)
        {
            pathNodesWithId[i].SetAttributeValue("data-info", types.ElementAt(i));
        }
    }

    private static void HideTextInLinks(HtmlDocument htmlDoc)
    {
        var gTags = htmlDoc.DocumentNode.SelectNodes("//g[starts-with(@id, 'link_')]");
        if (gTags != null)
        {
            foreach (var gTag in gTags)
            {
                var textTags = gTag.SelectNodes(".//text");
                if (textTags != null)
                {
                    foreach (var textTag in textTags)
                    {
                        textTag.SetAttributeValue("display", "none");
                    }
                }
            }
        }
    }

    private byte[] RunPlantUml(Stream plantUmlStream, string format)
    {
        return RunExternalProcess("java", $"-jar {PlantUmlJarPath} {format} -pipe", plantUmlStream);
    }

    private byte[] ConvertPngToPdf(byte[] pngBytes)
    {
        using var inputMemoryStream = new MemoryStream(pngBytes);
        return RunExternalProcess(ImageMagickPath, "png:- pdf:-", inputMemoryStream);
    }

    private byte[] RunExternalProcess(string fileName, string arguments, Stream inputStream)
    {
        using var process = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            }
        };

        process.Start();
        inputStream.CopyTo(process.StandardInput.BaseStream);
        process.StandardInput.Close();

        using var memoryStream = new MemoryStream();
        process.StandardOutput.BaseStream.CopyTo(memoryStream);
        process.WaitForExit();
        return memoryStream.ToArray();
    }

    private string GetTableName(IEntityType entityType)
    {
        return entityType.GetTableName();
    }

    private enum Format
    {
        Html,
        Pdf
    }
}
